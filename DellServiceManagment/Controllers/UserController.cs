﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DellServiceManagment.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UserController : Controller
    {
        [HttpGet]
       public IActionResult Register()
        {
            return Ok();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return Ok();
        }

        [Authorize]
        [HttpGet]
        public IActionResult Test()
        {
            return Ok();
        }
    }
}
